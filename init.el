;;; init.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;; This config start here
;;; Code:
;; (org-babel-load-file "~/.emacs.d/config.org")

(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs cargado en %s y listos para ser usado."
                     (format "%.2f segundos"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

(defvar default-file-name-handler-alist file-name-handler-alist)
(setq gc-cons-threshold 402653184
      gc-cons-percentage 0.6
      file-name-handler-alist nil)

;;
;; Carga de las variables y carpetas
;;

(require 'variables (concat user-emacs-directory "core/extra/variables.el"))

;;
;; Carga de todos los archivos dinamicamente
;;

(defun file-name-is-elisp? (file-name)
  "Say if the extension of a FILE NAME implies Emacs Lisp code.
This is done by checking if the extension is present in
`load-suffixes'."
  (let ((file-suffix (concat "." (downcase (file-name-extension file-name))))
        (emacs-lisp-suffixes (mapcar 'downcase load-suffixes)))
    (if (member file-suffix emacs-lisp-suffixes) t nil)))

(defun load-directory (directory &optional recursive)
  "Load the Emacs Lisp files in a DIRECTORY.
Operate recursively in its subdirectories if RECURSIVE is non-nil."
  (let ((visited nil))
    (dolist (name (directory-files directory t))
      (if (not (string-match "/\\.[^/]*$" name))
          (if (file-directory-p name)
              (when recursive (load-directory name t))
            (when (file-name-is-elisp? name)
              (let ((file (file-name-sans-extension name)))
                (unless (member file visited)
                  (push file visited)
                  (load file)))))))))

(load-directory (expand-file-name "core/" user-emacs-directory))
(load-directory (expand-file-name "modules/" user-emacs-directory) t)

(add-hook 'emacs-startup-hook
          (lambda ()
            "Restore defalut values after init."
            (setq gc-cons-threshold 16777216
                  gc-cons-percentage 0.1
                  file-name-handler-alist default-file-name-handler-alist)

            (add-hook 'focus-out-hook 'garbage-collect)))

(provide 'init)
;;; init.el ends here
