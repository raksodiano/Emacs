;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package smooth-scrolling
  :ensure t
  :config
  (setq smooth-scroll-margin 5))

(use-package highlight-indentation
  :ensure t
  :commands (highlight-indentation-mode highlight-indentation-current-column-mode)
  :config (add-hook 'prog-mode-hook 'highlight-indentation-mode))

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (setq which-key-sort-order 'which-key-key-order-alpha)
  (which-key-mode))


(provide 'config)
;;; config.el ends here
