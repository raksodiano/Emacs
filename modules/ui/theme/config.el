;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package doom-themes)
(load-theme 'doom-one t)

(provide 'config)
;;; config.el ends here
