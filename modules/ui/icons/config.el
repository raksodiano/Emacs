;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package all-the-icons
  :commands (all-the-icons-octicon
             all-the-icons-faicon
             all-the-icons-fileicon
             all-the-icons-wicon
             all-the-icons-material
             all-the-icons-alltheicon
             all-the-icons-install-fonts))

(use-package spaceline-all-the-icons
  :ensure t
  :after spaceline
  :init
  (setq spaceline-all-the-icons-separator-type 'none)
  :config
  (spaceline-all-the-icons-theme)
  (spaceline-toggle-all-the-icons-buffer-position-on)
  (spaceline-all-the-icons-theme 'python-pyvenv))

(provide 'config)
;;; config.el ends here
