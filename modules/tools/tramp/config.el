;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package tramp
  :config
  (setq tramp-persistency-file-name (concat tramp-dir "tramp")
        tramp-auto-save-directory (concat tramp-dir "tramp-autosave")
        tramp-debug-buffer nil
        tramp-verbose 10
        tramp-shell-prompt-pattern "\\(?:^\\|\r\\)[^]#$%>\n]*#?[]#$%>] *\\(^[\\[[0-9;]*[a-zA-Z] *\\)*")
  (tramp-set-completion-function "ssh" '((tramp-parse-sconfig "/etc/ssh_config")
                                         (tramp-parse-sconfig "~/.ssh/config"))))

(provide 'config)
;;; config.el ends here
