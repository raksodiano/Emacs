;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package multi-term
  :ensure t
  :config

  (custom-set-faces
   '(term ((t (:inherit default :foreground "#ffffff"))))
   '(term-color-black ((t (:background "#000000" :foreground "#31363b"))))
   '(term-color-blue ((t (:background "#2980b9" :foreground "#0099ff"))))
   '(term-color-green ((t (:background "#218058" :foreground "#27ae60"))))
   '(term-color-magenta ((t (:background "#8e44ad" :foreground "#af81ff"))))
   '(term-color-red ((t (:background "#c0392b" :foreground "#f44f4f"))))
   '(term-color-white ((t (:background "#acada1" :foreground "#cfd0c2"))))
   '(term-color-yellow ((t (:background "#fdbc4b" :foreground "#fdbc4b")))))

  (setq multi-term-program "/bin/bash"))

(provide 'config)
;;; config.el ends here
