;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package imenu-anywhere
  :ensure t)

(use-package imenu+
  :disabled ;; ver razón en https://redd.it/3ahj2d
  :init
  (setf imenup-ignore-comments-flag t))

(provide 'config)
;;; config.el ends here
