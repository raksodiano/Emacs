;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package shrink-path
  :ensure t)

(use-package neotree
  :ensure t
  :commands (neotree-show
             neotree-hide
             neotree-toggle
             neotree-dir
             neotree-find
             neo-global--with-buffer
             neo-global--window-exists-p)
  :bind (:map neotree-mode-map
              ("<C-return>" . neotree-change-root)
              ("C" . neotree-change-root)
              ("c" . neotree-create-node)
              ("+" . neotree-create-node)
              ("d" . neotree-delete-node)
              ("r" . neotree-rename-node))
  :config
  (setq neo-smart-open t
        neo-autorefresh t
        neo-mode-line-type 'neotree
        neo-show-updir-line nil
        neo-create-file-auto-open nil
        neo-auto-indent-point nil
        neo-mode-line-type 'none
        neo-window-width 25
        neo-theme (if (display-graphic-p) 'icons 'arrow)
        neo-banner-message nil
        neo-confirm-create-file #'off-p
        neo-confirm-create-directory #'off-p
        neo-hidden-regexp-list
        '(;; vcs folders
          "^\\.\\(git\\|hg\\|svn\\)$"
          ;; compiled files
          "\\.\\(pyc\\|o\\|elc\\|lock\\|css.map\\)$"
          ;; generated files, caches or local pkgs
          "^\\(node_modules\\|vendor\\|.\\(project\\|cask\\|yardoc\\|sass-cache\\)\\)$"
          ;; org-mode folders
          "^\\.\\(sync\\|export\\|attach\\)$"
          "~$"
          "^#.*#$"))

  (when (bound-and-true-p winner-mode)
    (push neo-buffer-name winner-boring-buffers))

  (defun shrink-root-entry (node)
    "shrink-print pwd in neotree"
    (insert (propertize (concat (shrink-path-dirs node) "\n") 'face `(:inherit (,neo-root-dir-face)))))

  (advice-add #'neo-buffer--insert-root-entry :override #'shrink-root-entry))

(defun neotree-project-dir-toggle ()
  "Open NeoTree using the project root, using find-file-in-project or the current buffer directory."
  (interactive)
  (let ((project-dir
         (ignore-errors
             ;;; Pick one: projectile or find-file-in-project
           (projectile-project-root)))
        (file-name (buffer-file-name))
        (neo-smart-open t))
    (if (and (fboundp 'neo-global--window-exists-p)
             (neo-global--window-exists-p))
        (neotree-hide)
      (progn
        (neotree-show)
        (if project-dir
            (neotree-dir project-dir))
        (if file-name
            (neotree-find file-name))))))

(provide 'config)
;;; config.el ends here
