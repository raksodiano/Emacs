;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package prodigy
  :ensure t)

(provide 'config)
;;; config.el ends here
