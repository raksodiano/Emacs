;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package flx
  :ensure t)

(use-package avy
  :ensure t)

(use-package ivy
  :ensure t
  :diminish ivy-mode
  :bind (:map ivy-mode-map)
  :config
  (ivy-mode 1)
  (setq ivy-wrap t
        ivy-virtual-abbreviate 'full
        ivy-use-virtual-buffers t
        ivy-count-format "(%d/%d) "
        ivy-re-builders-alist
        '((read-file-name-internal . ivy--regex-fuzzy)
          (t . ivy--regex-plus)))
  (setq ivy-on-del-error-function nil
        ivy-initial-inputs-alist nil))

(use-package swiper
  :bind
  (:map read-expression-map
        ("C-r" . counsel-expression-history)))

(use-package counsel
  :ensure t
  :config
  (setq counsel-find-file-at-point t))

(use-package imenu-anywhere
  :ensure t)

(provide 'config)
;;; config.el ends here
