;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package org-beautify-theme
  :disabled
  :after org)

(use-package org-bullets
  :ensure t
  :after org
  :init
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package org-download
  :ensure t
  :after org
  :init
  (setq-default org-download-image-dir "~/org/imagenes/"))

(use-package ob-restclient
  :ensure t
  :after org)

(use-package ob-translate
  :ensure t)

(use-package htmlize
  :ensure t
  :after org)

(use-package deft
  :ensure t
  :after org)

(use-package noflet
  :ensure t )

(use-package org
  :ensure t
  :pin org
  :config
  (progn
    (add-hook 'org-mode-hook
              (lambda ()
                (turn-on-auto-fill)
                (switch-dictionary)))

    (setq org-clock-persist 'history)
    (setq org-src-fontify-natively t)
    (setq org-enforce-todo-dependencies t)
    (setq org-html-validation-link nil)
    (org-clock-persistence-insinuate)
    (add-hook 'org-mode-hook (lambda ()
                               (org-indent-mode t)
                               (diminish 'org-indent-mode)))

    (org-babel-do-load-languages 'org-babel-load-languages
                                 '((python . t)
                                   (shell . t)
                                   (lisp . t)
                                   (sql . t)
                                   (restclient . t)
                                   (dot . t)
                                   (plantuml . t)
                                   (emacs-lisp . t)))

    ;; set the modules enabled by default
    (setq org-modules
          '(org-bbdb
            org-bibtex
            org-bullets
            toc-org
            org-download
            ob-restclient
            org-tree-slide
            org-docview
            org-mhe
            org-rmail
            org-crypt
            org-protocol
            org-pomodoro
            org-gnus
            org-id
            org-info
            org-habit
            org-irc
            org-annotate-file
            org-eval
            org-expiry
            org-man
            org-panel
            org-toc
            ox-md))

    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

  ;; set default ellipsis
  (setq org-bullets-bullet-list '("☣" "✡" "✽" "✲" "✱" "✻" "✽" "✾" "❁" "❂" "❃" "❄" "❅" "❆" "❇"))
  (setq org-ellipsis " ⤵")

  ;; set default directories
  (setq org-directory org-dir
        org-default-notes-file (concat org-directory "/notes.org"))

  ;; set the archive
  (setq org-archive-location "~/org/archive.org::datetree/** Archived")

  ;; highlight code blocks syntax
  (setq org-src-fontify-natively  t
        org-src-tab-acts-natively t)

  ;; more sane emphasis regex to export to HTML as substitute of Markdown
  (org-set-emph-re 'org-emphasis-regexp-components
                   '(" \t({"
                     "- \t.,:!?;)}[:multibyte:]"
                     " \t\r\n,"
                     "."
                     1))

  ;; tasks management
  (setq org-refile-targets '((org-agenda-files :maxlevel . 5)))
  (setq org-log-done t
        org-clock-idle-time nil
        org-todo-keywords (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                                  (sequence "WAITING(w)" "HOLD(h)" "|" "CANCELLED(c)" "PHONE" "MEETING"))))

  ;; agenda & diary
  (setq org-columns-default-format "%50ITEM(Task) %10CLOCKSUM %16TIMESTAMP_IA")
  (setq org-agenda-include-diary t
        org-agenda-tags-todo-honor-ignore-options t
        org-agenda-start-on-weekday 0
        org-agenda-timegrid-use-ampm 1
        org-agenda-inhibit-startup t
        org-agenda-files (quote ("~/org/work.org" "~/org/fromwork.org"
                                 "~/org/stuff.org" "~/org/fromstuff.org"
                                 "~/org/notes.org")))

  (eval-after-load 'eww
    '(progn (org-link-set-parameters "eww" :follow #'eww :store #'org-eww-store-link)))

  (setq org-agenda-custom-commands
        '(("Q" . "Custom queries") ;; gives label to "Q"
          ("Qa" "Archive search" search "" ((org-agenda-files (file-expand-wildcards "~/org/archive.org"))))
          ("n" todo "NEXT")
          ("w" todo "WAITING")
          ("d" "Agenda + Next Actions" ((agenda) (todo "NEXT")))))

  ;; date insertion configuration
  (setq org-expiry-created-property-name "CREATED"
        org-expiry-inactive-timestamps t
        org-todo-state-tags-triggers (quote (("CANCELLED" ("CANCELLED" . t))
                                             ("WAITING" ("WAITING" . t))
                                             ("HOLD" ("WAITING") ("HOLD" . t))
                                             (done ("WAITING") ("HOLD"))
                                             ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                                             ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                                             ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))
  ;; capture
  (setq org-capture-templates '(("w" "Work TODO" entry (file+olp "~/org/work.org" "Work" "Tasks") "* TODO %? \nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n:PROPERTIES:\n:CATEGORY: TASKS\n:CREATED: %U\n:END:")
                                ("o" "Work Overtime" entry (file+olp "~/org/work.org" "Work" "COMMENT Overtime") "* %? \nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n:PROPERTIES:\n:CREATED: %U\n:END:")
                                ("m" "Work Meetings" entry (file+olp "~/org/work.org" "Work" "Meetings") "* %? \nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n:PROPERTIES:\n:CATEGORY: MEETINGS\n:CREATED: %U\n:END:")
                                ("t" "Work Training's" entry (file+olp "~/org/work.org" "Work" "Training's") "* %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n:PROPERTIES:\n:CATEGORY: TRAINING'S\n:CREATED: %U\n:END:")
                                ("S" "Stuff TODO" entry (file+olp "~/org/stuff.org" "Stuff" "Tasks") "* TODO %? \n:PROPERTIES:\n:CATEGORY: TASKS\n:CREATED: %U\n:END:")
                                ("M" "Stuff Meetings" entry (file+olp "~/org/stuff.org" "Stuff" "Meetings") "* %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n:PROPERTIES:\n:CATEGORY: MEETINGS\n:CREATED: %U\n:END:")
                                ("T" "Stuff Training's" entry (file+olp "~/org/stuff.org" "Stuff" "Training's") "* %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n:PROPERTIES:\n:CATEGORY: TRAINING'S\n:CREATED: %U\n:END:")))

  ;; configure the external apps to open files
  (add-to-list (quote org-file-apps)
               (quote ("\\.pdf\\'" lambda (file link) (org-pdfview-open link))))

  ;; protect hidden trees for being inadvertily edited (do not work with evil)
  (setq-default org-catch-invisible-edits  'error
                org-ctrl-k-protect-subtree 'error)

  ;; show images inline
  ;; only works in GUI, but is a nice feature to have
  (when (window-system)
    (setq org-startup-with-inline-images t))
  ;; limit images width
  (setq org-image-actual-width '(800))

  ;; :::::: Org-Babel ::::::

  ;; languages supported
  (org-babel-do-load-languages
   (quote org-babel-load-languages)
   (quote ((calc . t)
           (dot . t)
           (emacs-lisp . t)
           (latex . t)
           (org . t)
           (makefile . t)
           (plantuml . t)
           (python . t)
           (ruby . t)
           (shell . t)
           (sqlite . t)
           (sql . t))))
  (setq org-babel-python-command "python3")

  ;; refresh images after execution
  (add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images)

  ;; don't ask confirmation to execute "safe" languages
  (defun org-confirm-babel-evaluate (lang body)
    (and (not (string= lang "ditaa"))
         (not (string= lang "dot"))
         (not (string= lang "gnuplot"))
         (not (string= lang "ledger"))
         (not (string= lang "plantuml"))))
  (setq org-confirm-babel-evaluate 'org-confirm-babel-evaluate))

;; Disable flycheck when edit org-src-block
(defun disable-fylcheck-in-org-src-block ()
  (setq-local flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(add-hook 'org-src-mode-hook 'disable-fylcheck-in-org-src-block)

(defun org-insert-file (filename)
  "Insert Elisp code block recreating file named FILENAME."
  (interactive "f")
  (let ((base64-string
         (with-temp-buffer
           (insert-file-contents-literally filename)
           (base64-encode-region (point-min) (point-max))
           (buffer-string))))
    (insert (format "#+BEGIN_SRC emacs-lisp :results output silent\n  (with-temp-file %S\n    (insert (base64-decode-string\n      %S)))\n#+END_SRC" filename base64-string))))

(setq deft-directory org-dir)
(setq deft-extension '("txt" "tex" "org"))
(setq deft-text-mode 'org-mode)
(setq deft-use-filename-as-title t)
(setq deft-auto-save-interval 0)

(setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
(setq org-reveal-mathjax t)

(provide 'config)
;;; config.el ends here
