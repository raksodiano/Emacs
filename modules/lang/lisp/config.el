;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package slime
  :ensure t
  :init (slime-setup))

(setq inferior-lisp-program "sbcl")
(setq slime-auto-connect 'ask)

(defun my-slime-setup ()
  (require 'slime)
  (slime-setup))
(defvar my--slime-setup-done nil)
(defun my-slime-setup-once ()
  (unless my--slime-setup-done
    (my-slime-setup)
    (setq my--slime-setup-done t)))
(defadvice lisp-mode (before my-slime-setup-once activate)
  (my-slime-setup-once))

(provide 'config)
;;; config.el ends here
