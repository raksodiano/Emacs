;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package latex
  :ensure auctex
  :init
  (setf TeX-auto-save t
        TeX-parse-self t
        TeX-save-query nil
        TeX-newline-function 'newline-and-indent
        LaTeX-item-indent 0)
  (setq-default TeX-master nil)
  (setq-default TeX-engine 'xetex)
  (setq-default TeX-PDF-mode t)
  (add-hook 'LaTeX-mode-hook 'yas-minor-mode-on))
(use-package reftex
  :init
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)   ; with AUCTeX LaTeX mode
  (add-hook 'latex-mode-hook 'turn-on-reftex))   ; with Emacs latex mode

(use-package tex
  :defer t
  :ensure auctex
  :bind
  ("C-c c" . TeX-clean)
  :init
  (progn
    (require 'smartparens-latex)
    (add-hook 'TeX-mode-hook
              (lambda ()
                (outline-minor-mode t)
                (flyspell-mode t)
                (TeX-PDF-mode t)
                (TeX-fold-mode t)
                (switch-dictionary)))
    (add-hook 'LaTeX-mode-hook
              (lambda ()
                (LaTeX-math-mode t)
                (reftex-mode t)))
    (setq TeX-auto-save t
          TeX-parse-self t
          LaTeX-syntactic-comment t
          TeX-save-query nil
          TeX-PDF-mode t
          TeX-auto-untabify t)
    (setq-default TeX-engine 'xetex))
  :config
  (add-hook 'TeX-mode-hook 'turn-on-auto-fill)
  (add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
  (use-package cdlatex
    :ensure t)
  (add-hook 'LaTeX-mode-hook 'turn-on-cdlatex)

  ;; Use pdf-tools to open PDF files
  (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
        TeX-source-correlate-start-server t)

  ;; Update PDF buffers after successful LaTeX runs
  (add-hook 'TeX-after-TeX-LaTeX-command-finished-hook
            #'TeX-revert-document-buffer)

  (setq tex-command "pdftex")

                                        ;(setq TeX-view-program-list '(("zathura" "zathura %o"))
                                        ;      TeX-view-program-selection '((output-pdf "zathura")))

  ;; set XeTeX mode in TeX/LaTeX
  (add-hook 'LaTeX-mode-hook
            (lambda () (push
                        '("cTeX" "%(ctex_bin) %t " TeX-run-TeX nil t
                          :help "Compilation with custom script") TeX-command-list)
              (add-to-list 'TeX-expand-list
                           '("%(ctex_bin)" (lambda ()
                                             (concat bin-dir "ctex" ))))
              (setq TeX-command-default "cTeX"
                    TeX-save-query nil
                    TeX-show-compilation t))))

(use-package reftex
  :commands turn-on-reftex
  :init
  (progn
    (setq reftex-plug-into-AUCTeX t)))

(use-package bibtex
  :init
  (progn
    (setq bibtex-align-at-equal-sign t)
    (add-hook 'bibtex-mode-hook
              (lambda ()
                (set-fill-column 120)))))

(use-package company-auctex
  :ensure t
  :config
  (defun cfg:TeX-mode-hook ()
    (company-auctex-init))
  (add-hook 'LaTeX-mode-hook 'cfg:TeX-mode-hook)
  (add-hook 'TeX-mode-hook 'cfg:TeX-mode-hook))

(provide 'config)
;;; config.el ends here
