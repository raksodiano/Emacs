;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package php-refactor-mode
  :after (php-mode)
  :config
  (add-hook 'php-mode-hook 'php-refactor-mode))

(use-package php-mode
  :ensure t)

(add-hook 'php-mode-hook 'cfg/php-mode-hook)

(provide 'config)
;;; config.el ends here
