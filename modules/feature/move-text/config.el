;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package move-text
  :config (move-text-default-bindings))

(provide 'config)
;;; config.el ends here
