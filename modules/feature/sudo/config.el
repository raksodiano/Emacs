;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package sudo-edit
  :ensure t)

(provide 'config)
;;; config.el ends here
