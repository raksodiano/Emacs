;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package golden-ratio
  :ensure t
  :config
  (setq
   ;; golden-ratio-auto-scale t
   golden-ratio-adjust-factor 1
   golden-ratio-wide-adjust-factor 1
   golden-ratio-exclude-modes '("dired-mode"
                                "ediff-mode"
                                "eshell-mode"
                                "neotree-mode"
                                "sr-speedbar-mode"))

  (golden-ratio-mode 1))

(use-package ace-window
  :init
  (custom-set-faces
   '(aw-leading-char-face
     ((t (:inherit ace-jump-face-foreground :height 3.0)))))

  (defun --count-frames ()
    "Return the number of visible frames"
    (let* ((frames (if (daemonp) (butlast (visible-frame-list) 1) (visible-frame-list))))
      (length frames)))

  (defun other-window ()
    "Change the cursor's focus to another window"
    (interactive)
    (if (or (> (count-windows) 2) (> (--count-frames) 1))
        (let ((golden-ratio-state golden-ratio-mode))
          (when golden-ratio-state
            (golden-ratio-mode -1)
            (balance-windows))
          (ace-window 1)
          (when golden-ratio-state
            (golden-ratio-mode 1)
            (golden-ratio)))
      (progn
        (ace-window 0)
        (golden-ratio))))
  :config
  (setf aw-keys '(?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9)))

(provide 'config)
;;; config.el ends here
