;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package autoinsert
  :init
  (setq auto-insert-directory (concat user-emacs-directory "modules/feature/auto-insertion/templates/"))
  (setq auto-insert-query nil)
  (add-hook 'find-file-hook 'auto-insert)
  (auto-insert-mode 1))

;; (define-auto-insert "\\.el?$" "default.el")
(define-auto-insert "\\.html?$" "default.html")
(define-auto-insert "\\.php?$" "default.php")
;; (define-auto-insert "\\.rb?$" "default.rb")
;; (define-auto-insert "\\.sh?$" "default.sh")
;; (define-auto-insert "\\.sh?$" "default.sh")

(provide 'config)
;;; config.el ends here
