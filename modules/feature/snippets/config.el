;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package yasnippet
  :ensure t
  :defer 2
  :config
  (yas-global-mode))

(use-package yasnippet-snippets
  :ensure t)

(setq yas-snippet-dirs
      '(snippets-dir
        yasnippet-snippets-dir))

(provide 'config)
;;; config.el ends here
