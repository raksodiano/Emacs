;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package subword
  :ensure nil
  :diminish subword-mode)

(provide 'config)
;;; config.el ends here
