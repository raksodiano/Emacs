;;; config.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package highlight-indent-guides
  :init
  (setf highlight-indent-guides-method 'fill)
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode))

(provide 'config)
;;; config.el ends here
