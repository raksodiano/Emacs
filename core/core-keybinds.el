;;; keybinds.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; Teclas de edición en texto
(global-set-key (kbd "M-;") 'comment-dwim-2)
(global-set-key (kbd "M-p") 'backward-paragraph)
(global-set-key (kbd "M-n") 'forward-paragraph)
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-c D") 'duplicate-current-line-or-region) ;; Duplicar linea

;; Flycheck
(global-set-key (kbd "C-c e p") 'flycheck-previous-error)
(global-set-key (kbd "C-c e n") 'flycheck-next-error)

;; Flyspell
(global-set-key (kbd "C-c t s") 'flyspell-mode)
(global-set-key (kbd "C-c t b") 'flyspell-buffers)

;; Smartparens
(global-set-key (kbd "C-}") 'sp-forward-slurp-sexp)
(global-set-key (kbd "M-s") 'sp-backward-unwrap-sexp)
(global-set-key (kbd "C-c [") 'sp-select-next-thing)
(global-set-key (kbd "C-c ]") 'sp-select-next-thing-exchange)

;; Tamaño de fuente
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;; Cambio de diccionario
(global-set-key (kbd "M-i") 'switch-dictionary)

;; IVY
(global-set-key (kbd "C-'") 'ivy-avy)

;; Imenu
(global-set-key (kbd "C-c i") 'ivy-imenu-anywhere)

;; Swiper
(global-set-key (kbd "C-: r") 'swiper)
(global-set-key (kbd "C-: s") 'swiper)
(global-set-key (kbd "C-: a") 'swiper-all)

;; imenu-anywhere
(global-set-key (kbd "C-c .") 'ivy-imenu-anywhere)

;; Recompilación
(global-set-key (kbd "C-,") 'recompile)

;; Latex
(global-set-key (kbd "C-c c") 'TeX-clean)

;; Free-keys
(global-set-key (kbd "C-h C-k") 'free-keys)

;; Prodigy
(global-set-key (kbd "C-x q") 'prodigy)

;; Manejo de Buffers
(global-set-key (kbd "C-x 1") 'zygospore-toggle-delete-other-windows)
(global-set-key (kbd "C-x k") 'kill-buffer-and-window)

;; Redimencionar Buffers
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'enlarge-window)
(global-set-key (kbd "S-C-<up>") 'shrink-window)

(global-set-key (kbd "C-x o") 'other-window)

(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "M-y") 'counsel-yank-pop)
(global-set-key (kbd "M-SPC") 'counsel-shell-history)
(global-set-key (kbd "C-c b") 'counsel-imenu)
(global-set-key (kbd "C-h f") 'counsel-describe-function)
(global-set-key (kbd "C-h v") 'counsel-describe-variable)
(global-set-key (kbd "C-h b") 'counsel-descbinds)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "C-x r r") 'counsel-rg)

(global-set-key [f2] 'neotree-toggle)
(global-set-key [f5] 'revert-buffer)
(global-set-key [f8] 'package-utils-upgrade-all-and-restart)

(global-set-key (kbd "C-x m") 'multi-term)
(global-set-key (kbd "<S-down>") 'multi-term)
(global-set-key (kbd "<S-left>") 'multi-term-prev)
(global-set-key (kbd "<S-right>") 'multi-term-next)
(global-set-key (kbd "C-<backspace>") 'term-send-backward-kill-word)
(global-set-key (kbd "C-<delete>") 'term-send-forward-kill-word)
(global-set-key (kbd "C-<left>") 'term-send-backward-word)
(global-set-key (kbd "C-<right>") 'term-send-forward-word)
(global-set-key (kbd "C-c C-j") 'term-line-mode)
(global-set-key (kbd "C-c C-k") 'term-char-mode)
(global-set-key (kbd "C-v") 'scroll-up)
(global-set-key (kbd "C-x y") 'term-paste)
(global-set-key (kbd "C-z") 'term-stop-subjob)
(global-set-key (kbd "M-DEL") 'term-send-backward-kill-word)
(global-set-key (kbd "M-d") 'term-send-forward-kill-word)

(global-set-key (kbd "C-: c c") 'mc/edit-lines)
(global-set-key (kbd "C-: c n") 'mc/mark-next-like-this)
(global-set-key (kbd "C-: c N") 'mc/skip-to-next-like-this)
(global-set-key (kbd "C-: c p") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-: c P") 'mc/skip-to-previous-like-this)

(global-set-key (kbd "C-c a") 'org-agenda-list)
(global-set-key (kbd "C-c c") 'org-capture)

(global-set-key (kbd "C-x u") 'undo-tree-visualizer)

(provide 'keybinds)
;;; keybinds.el ends here
