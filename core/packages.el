;;; packages.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package package-utils
  :ensure t)

(use-package bug-hunter
  :ensure t)

(use-package esup
  :ensure t
  :commands esup)

(use-package keyfreq
  :if (daemonp)
  :config
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1))

(use-package moz-controller
  :ensure t)

(use-package vlf-setup
  :ensure vlf
  :init
  (setf vlf-application 'dont-ask))

(use-package validate
  :ensure t)

(use-package diminish
  :ensure t)

(use-package mwe-log-commands
  :ensure t)

(use-package visual-fill-column
  :ensure t)

(use-package paren
  :init
  (show-paren-mode)
  :config
  (set-face-background 'show-paren-match (face-background 'default))
  (set-face-foreground 'show-paren-match "#def")
  (set-face-attribute 'show-paren-match nil :weight 'extra-bold))

(use-package smartparens
  :ensure t
  :commands
  (smartparens-mode
   smartparens-strict-mode)
  :bind
  (:map smartparens-strict-mode-map)
  :config
  (require 'smartparens-config))

(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package zygospore
  :ensure t)

(use-package paredit
  :ensure t)

(use-package package-lint
  :ensure t)

(use-package multiple-cursors
  :ensure t)

(use-package comment-dwim-2
  :ensure t)

(use-package nlinum
  :ensure t
  :config
  (setq nlinum-highlight-current-line t)
  (add-hook 'prog-mode-hook 'nlinum-mode))

(use-package eldoc
  :ensure nil
  :diminish eldoc-mode
  :init
  (setf eldoc-idle-delay 1.0))

(use-package recentf
  :config
  (setq recentf-save-file (concat cache-dir "recentf")
        recentf-exclude '("/tmp/" "/ssh:" "/sudo:" "/scp:" "/scpx:" "/ssh:")
        recentf-max-saved-items 15
        recentf-max-menu-items 15
        recentf-auto-cleanup 'never)
  (add-to-list 'recentf-exclude etc-dir)
  (add-to-list 'recentf-exclude cache-dir)
  (recentf-mode +1))

(use-package aggressive-indent
  :diminish aggressive-indent-mode
  :config
  (add-to-list
   'aggressive-indent-dont-indent-if
   '(and (derived-mode-p 'c++-mode)
         (null (string-match "\\([;{}]\\|\\b\\(if\\|for\\|while\\)\\b\\)"
                             (thing-at-point 'line)))))
  (global-aggressive-indent-mode 1)
  (add-to-list 'aggressive-indent-excluded-modes 'html-mode))

(use-package docker
  :ensure t)

(use-package docker-compose-mode
  :ensure t)

(use-package dockerfile-mode
  :mode "Dockerfile\\'")

(use-package free-keys
  :ensure t)

(use-package editorconfig
  :ensure t
  :diminish ""
  :config
  (editorconfig-mode 1))

;; (use-package w3m
;;   :ensure t)

(provide 'packages)
;;; packages.el ends here
