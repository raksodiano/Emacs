;;; core-packages.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(setq package--init-file-ensured t
      package-user-dir (expand-file-name "elpa" packages-dir)
      package-enable-at-startup nil
      package-archives '(("gnu"   . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("marmalade" . "http://marmalade-repo.org/packages/"))

      package-archive-priorities '(("org" . 10)
                                   ("melpa" . 7)
                                   ("gnu" . 5)
                                   ("marmalade" . 3)))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(provide 'core-packages)
;;; core-packages.el ends here
