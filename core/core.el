;;; core.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(setq history-length 1000
      history-delete-duplicates t
      savehist-save-minibuffer-history 1
      savehist-additional-variables '(kill-ring
                                      search-ring
                                      regexp-search-ring))

(savehist-mode t)

(when (fboundp 'set-charset-priority)
  (set-charset-priority 'unicode))
(prefer-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

(setq-default frame-title-format (list "Emacs"))

(setq inhibit-startup-screen t
      initial-scratch-message nil
      large-file-warning-threshold 100000000
      ;; Scroll
      scroll-step 1
      scroll-conservatively 3000
      scroll-up-aggressively 0.02
      scroll-down-aggressively 0.02)

(add-hook 'window-setup-hook 'toggle-frame-maximized t)

(setq-default indent-tabs-mode nil
              tab-width 2)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)
(toggle-indicate-empty-lines)
(delete-selection-mode)
(blink-cursor-mode -1)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(set-default 'truncate-lines t)
(setq visual-line-mode t)
(global-eldoc-mode -1)
(electric-pair-mode 1)

(setq major-mode 'text-mode
      current-language-environment "Spanish")

(setq-default ad-redefinition-action 'accept
              apropos-do-all t
              compilation-always-kill t
              compilation-ask-about-save nil
              compilation-scroll-output t
              confirm-nonexistent-file-or-buffer t
              enable-recursive-minibuffers nil
              idle-update-delay 2
              minibuffer-prompt-properties '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)
              auto-save-default nil
              auto-save-list-file-prefix nil
              make-backup-files nil
              create-lockfiles nil)

(setq-default bidi-display-reordering nil
              blink-matching-paren nil
              cursor-in-non-selected-windows nil
              display-line-numbers-width 3
              frame-inhibit-implied-resize t
              fringe-indicator-alist (delq
                                      (assq 'continuation fringe-indicator-alist)
                                      fringe-indicator-alist)
              highlight-nonselected-windows nil
              image-animate-loop t
              indicate-buffer-boundaries nil
              indicate-empty-lines nil
              max-mini-window-height 0.3
              mode-line-default-help-echo nil
              mouse-yank-at-point t
              ibuffer-use-other-window t
              resize-mini-windows 'grow-only
              show-help-function nil
              split-width-threshold 160
              uniquify-buffer-name-style 'forward
              use-dialog-box nil
              visible-cursor nil
              x-stretch-cursor nil
              jit-lock-defer-time nil
              jit-lock-stealth-nice 0.1
              jit-lock-stealth-time 0.2
              jit-lock-stealth-verbose nil
              pos-tip-internal-border-width 6
              pos-tip-border-width 1
              ring-bell-function #'ignore
              visible-bell nil)

;;
;; Functions
;;

(defun auto-revert-mode ()
  "Recargar todos los buffers abiertos."
  (interactive)
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when (and (buffer-file-name) (file-exists-p (buffer-file-name)) (not (buffer-modified-p)))
        (revert-buffer t t t) )))
  (message "Refreshed open files."))

(defun duplicate-current-line-or-region (arg)
  "Duplicates the current line or region ARG times."
  (interactive "p")
  (let (beg end (origin (point)))
    (if (and mark-active (> (point) (mark)))
        (exchange-point-and-mark))
    (setq beg (line-beginning-position))
    (if mark-active
        (exchange-point-and-mark))
    (setq end (line-end-position))
    (let ((region (buffer-substring-no-properties beg end)))
      (dotimes (i arg)
        (goto-char end)
        (newline)
        (insert region)
        (setq end (point)))
      (goto-char (+ origin (* (length region) arg) arg)))))

(defun empty-buffer? ()
  "Empty Buffer."
  (= (buffer-end 1) (buffer-end -1)))

(provide 'core)
;;; core.el ends here
