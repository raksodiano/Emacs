;;; variables.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(defvar org-dir  "~/org/"
  "Archivos de configuración ORG.")

(defvar workspace-dir "~/Workspace/"
  "Carpeta de trabajos y practicas.")

(defvar backup-dir (concat user-emacs-directory ".backup/")
  "Aquí se guardan los backups generados.")

(defvar cache-dir (concat user-emacs-directory ".cache/")
  "Archivos volatiles.")

(defvar etc-dir (concat user-emacs-directory ".etc/")
  "Archivos de uso continuo.")

(defvar local-dir (concat user-emacs-directory ".local/")
  "Carpeta local para almacenar archivos que no se tocaran.")

(defvar backup-tramp-dir (concat backup-dir "tramp/")
  "Archivos tramp.")

(defvar cache-server-dir (concat cache-dir "server/")
  "Almacen de server's.")

(defvar tramp-dir (concat cache-dir "tramp/")
  "Tramp.")

(defvar cache-tramp-dir (concat cache-dir "tramp-auto-save/")
  "Sesiones ssh guardadas.")

(defvar cache-url-dir (concat cache-dir "url/")
  "URL's volatiles.")

(defvar shared-game-score-dir (concat etc-dir "shared-game-score/")
  "Score de los juegos.")

(defvar snippets-dir (concat etc-dir "snippets/")
  "Snippets guardados.")

(defvar url-dir (concat etc-dir "url/")
  "Almacen de URL's importantes.")

(defvar yasnippet-snippets-dir (concat etc-dir "yasnippet-snippets/")
  "Yasnippet-Snippets guardados.")

(defvar bookmark-dir (concat local-dir "bookmark/")
  "Bookmark Save.")

(defvar packages-dir (concat local-dir "packages/")
  "Paquetes instalados.")

(defvar projectile-dir (concat local-dir "projectile/")
  "Projectile Save.")

(dolist
    (dir
     (list org-dir
           workspace-dir
           backup-dir
           cache-dir
           etc-dir
           local-dir
           backup-tramp-dir
           cache-server-dir
           tramp-dir
           cache-tramp-dir
           cache-url-dir
           shared-game-score-dir
           snippets-dir
           url-dir
           yasnippet-snippets-dir
           bookmark-dir
           packages-dir
           projectile-dir))

  (unless (file-directory-p dir)
    (make-directory dir t)))

(setq-default
 backup-directory-alist (list (cons ".*" backup-dir))
 auto-save-list-file-prefix backup-dir
 auto-save-file-name-transforms `((".*" , backup-dir t))
 tramp-backup-directory-alist backup-tramp-dir)

(setq-default
 auto-save-list-file-name (concat cache-dir "autosave")
 semanticdb-default-save-directory (concat cache-dir "semanticdb")
 savehist-file (concat cache-dir "history")
 eshell-directory-name (concat cache-dir "eshell" ))

(setq-default
 server-auth-dir cache-server-dir)

(setq-default
 tramp-auto-save-directory tramp-dir
 tramp-persistency-file-name (concat tramp-dir "tramp")
 tramp-persistency-file-name (concat tramp-dir "tramp-persistency.el"))

(setq-default
 tramp-auto-save-directory cache-tramp-dir)

(setq-default
 url-cache-directory cache-url-dir
 url-configuration-directory (concat cache-url-dir "url"))

(setq custom-file (concat etc-dir "custom.el"))
(load custom-file t t)

(setq-default
 shared-game-score-directory shared-game-score-dir
 gamegrid-user-score-file-directory shared-game-score-dir)

(setq-default
 url-configuration-directory url-dir)

(provide 'variables)
;;; variables.el ends here
