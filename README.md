Configuración de Emacs
======================

Mi configuración de GNU Emacs, haciendo ajustes eventualmente :v

Paquetes Requeridos
-----------------------
- Hunspell
  * Usado para los diccionarios

- PIP
  * Programación en Python

- LaTeX
  * Usado para la generación de archivos PDF's a través de org-mode

Instalación
-----------

```bash
git clone https://gitlab.com/raksodiano/Emacs.git ~/.emacs.d/
```

Emacs como demonio (opcional)
-------------------------------------

``` bash
$ emacs --daemon
```
